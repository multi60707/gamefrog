function Frog() {
  tFrog = new Sprite(scene, "frog.png", 60, 60);
  //Properties
  tFrog.setSpeed(0)
  tFrog.setAngle(0)
  var speed = 0;
  //method
  tFrog.motionFrog = function () {
    
    if (keysDown[K_UP]) { //ขึ้น
     this.changeSpeedBy(1)
      if (this.speed > 10) {
        this.setSpeed(10)
      }
      //console.log("Up:"+frong.speed)

    } else
      if (keysDown[K_DOWN]) { //ลง
        this.changeSpeedBy(-1)
        if (this.speed < -3) {
          this.setSpeed(-3)
        }
        // console.log("Down:"+frong.speed)

      } else
        if (keysDown[K_LEFT]) { //ซ้าย
          this.changeAngleBy(-5)


        }
        else if (keysDown[K_RIGHT]) { //ขวา
          this.changeAngleBy(5)
        }
        else if (keysDown[K_SPACE]) {
          this.changeSpeedBy(-1)
          if (this.speed < 0) {
            this.setSpeed(0)
          }
        }

  }

  return tFrog;
}

function Fly(){
  tFly = new Sprite(scene,"fly.png",15,15);
  tFly.setSpeed(5)
  tFly.motionFly = function () {
    newDir =(Math.random()*90)-45
    this.changeAngleBy(newDir)
    
  }
  tFly.dead =function(){
    newX =Math.random()*this.cWidth
    newY =Math.random()*this.cHeight
    this.setPosition(newY,newY)
  }
  return tFly
}